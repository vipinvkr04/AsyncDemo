package com.example.demo;



import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.web.exchanges.HttpExchange;
import org.springframework.boot.actuate.web.exchanges.HttpExchangeRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.resource.HttpResource;

import lombok.extern.slf4j.Slf4j;

//https://filia-aleks.medium.com/microservice-performance-battle-spring-mvc-vs-webflux-80d39fd81bf0
@RestController
@Slf4j
public class TodoController {
    @Autowired
    private TodoService todoService;
   



    @GetMapping("/async1")
    public CompletableFuture<List<Todo>> performAsyncOperation1() {
    	 log.info("Start - fething todos performAsyncOperationWithCompletableFutureSupplyAsync");
    	 CompletableFuture<List<Todo>> result =  todoService.performAsyncOperationWithCompletableFutureSupplyAsync();
    	 log.info("End - fething todos performAsyncOperationWithCompletableFutureSupplyAsync");
         return result;
    }

    @GetMapping("/async2")
    public CompletableFuture<List<Todo>> performAsyncOperation2() {
    	 log.info("Start - fething todos performAsyncOperationWithCompletableFutureCompletedFuture");
    	CompletableFuture<List<Todo>> result = todoService.performAsyncOperationWithCompletableFutureCompletedFuture();
    	 log.info("End - fething todos performAsyncOperationWithCompletableFutureCompletedFuture");
        return result;
    }
    
    @GetMapping("/async3")
    public CompletableFuture<List<Todo>> performAsyncNonBlockingWebFlux() {
    	 log.info("Start - fething todos performAsyncNonBlockingWebFlux");
    	CompletableFuture<List<Todo>> result = todoService.performAsyncNonBlockingWebFlux();
    	 log.info("End - fething todos performAsyncNonBlockingWebFlux");
        return result;
    }
}
