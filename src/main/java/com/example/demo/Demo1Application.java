package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@EnableAsync
@Slf4j
public class Demo1Application {
	
	@Bean("threadPoolTaskExecutor")
	public TaskExecutor getAsyncExecutor() {
	    ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
	    executor.setCorePoolSize(20);
	    executor.setMaxPoolSize(100);
	    executor.setQueueCapacity(150);
	    executor.setWaitForTasksToCompleteOnShutdown(true);
	    executor.setThreadNamePrefix("Async-");
	    return executor;
	}
	
    public static void main(String[] args) {
        SpringApplication.run(Demo1Application.class, args);
    }
}


