package com.example.demo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.apache.commons.logging.Log;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.extern.slf4j.Slf4j;


@Service
@Slf4j
public class TodoService {
	
	 RestTemplate restTemplate = new RestTemplate();
	 private final WebClient webClient;
	 public TodoService() {
	        this.webClient = WebClient.builder().baseUrl("https://jsonplaceholder.typicode.com").build();
	    }
	 @Async("threadPoolTaskExecutor")
	 //supplyAsync() - to perform  computation asynchronously
	    public CompletableFuture<List<Todo>> performAsyncOperationWithCompletableFutureSupplyAsync() {
		 log.info("Async - fething todos performAsyncOperationWithCompletableFutureSupplyAsync");
	        return CompletableFuture.supplyAsync(() -> {
	        	 List<Todo> todos = new ArrayList<>();
  
	            String url = "https://jsonplaceholder.typicode.com/todos";
	            ResponseEntity<Todo[]> responseEntity = restTemplate.getForEntity(url, Todo[].class);
				todos = Arrays.asList(responseEntity.getBody());
	            return todos;
	        });
	    }

	    @Async("threadPoolTaskExecutor")
	    //completedFuture() - to create a completed CompletableFuture object with a specific value.
	    public CompletableFuture<List<Todo>> performAsyncOperationWithCompletableFutureCompletedFuture() {
	    	 log.info("Async - fething todos performAsyncOperationWithCompletableFutureCompletedFuture");
	    	 List<Todo> todos = new ArrayList<>();
	    	 String url = "https://jsonplaceholder.typicode.com/todos";
	    	 ResponseEntity<Todo[]> responseEntity = restTemplate.getForEntity(url, Todo[].class);
				todos = Arrays.asList(responseEntity.getBody());

	        return CompletableFuture.completedFuture(todos);
	    }
	    
	    @Async("threadPoolTaskExecutor")
	    public CompletableFuture<List<Todo>> performAsyncNonBlockingWebFlux() {
	    	 log.info("Async - fething todos performAsyncNonBlockingWebFlux");
	        	 //String url = "https://jsonplaceholder.typicode.com/todos";
	        	 CompletableFuture<List<Todo>> todos = webClient.get()
					        .uri("/todos")
					        .retrieve()
					        .bodyToMono(Todo[].class)
					        .toFuture()
					        .thenApply(Arrays::asList);
	        	 
					return todos;	
	    }
   
}



